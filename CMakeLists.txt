cmake_minimum_required(VERSION 3.2)

set(PROJECT_NAME MthWatering)

set(${PROJECT_NAME}_BOARD nano328)
#set(${PROJECT_NAME}_BOARD uno)

set(CMAKE_ARDUINO_PATH ~/arduino/sdk)
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_ARDUINO_PATH}/cmake/ArduinoToolchain.cmake)
set(CMAKE_EXTERNAL_LIBS_PATH ~/arduino/libraries)
link_directories(${CMAKE_EXTERNAL_LIBS_PATH})

project(${PROJECT_NAME})

set(${PROJECT_NAME}_SRCS ${PROJECT_NAME}.cpp)

if (${PROJECT_NAME}_BOARD MATCHES uno)
    set(${PROJECT_NAME}_PORT /dev/ttyACM0)
    set(uno.upload.speed 9600)
elseif (${PROJECT_NAME}_BOARD MATCHES nano328)
    set(${PROJECT_NAME}_PORT /dev/ttyUSB0)
endif ()

add_definitions(-DSERIAL_ENABLED)

set(${PROJECT_NAME}_AFLAGS -v)

generate_arduino_firmware(${PROJECT_NAME})
