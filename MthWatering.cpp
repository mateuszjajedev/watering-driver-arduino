#include <Arduino.h>
#include "mth_headers.h"
#include "mth_leds.h"
#include "mth_println.h"
#include "watering_support.h"

// PINS
#define SOIL_SENSOR_PIN  1
#define PUMP_PIN  10

// constants
#define MOISTURE_LEVEL_PUMP  400
#define IGNORE_SENSOR_LEVEL_COSIDER_NOT_MOUNTED 1000
#define PIPE_LENGTH 250 // in centimeters
#define WATERING_TIME 400 // millis ofc
//#define TIME_FOR_FILLING_PIPE 200 // in millis
#define TIME_TO_WAIT_AFTER_WATERING 120 // in seconds

const unsigned long TIME_FOR_FILLING_PIPE = pipeFillingTime(PIPE_LENGTH);
const unsigned long PUMP_POWER_TIME = TIME_FOR_FILLING_PIPE + WATERING_TIME;
const unsigned long TIME_TO_WAIT_AFTER_WATERING_MILLIS = TIME_TO_WAIT_AFTER_WATERING * 1000;

unsigned long time;
int moistureSensorValue;
unsigned long lastWateringTime = 0;

int leds[] = {2, 3, 4, 5, 6, 7, 8};
int ledsN = sizeof(leds) / sizeof(leds[0]);

void readSensor() {
    moistureSensorValue = analogRead(SOIL_SENSOR_PIN);
    println("Sensor value: ", moistureSensorValue);
}

void showMoistureOnLEDs() {
    if (moistureSensorValue >= 700) powerLEDs(7, leds, ledsN);
    if ((moistureSensorValue < 700) && (moistureSensorValue >= 600)) powerLEDs(6, leds, ledsN);
    if ((moistureSensorValue < 600) && (moistureSensorValue >= 500)) powerLEDs(5, leds, ledsN);
    if ((moistureSensorValue < 500) && (moistureSensorValue >= 400)) powerLEDs(4, leds, ledsN);
    if ((moistureSensorValue < 400) && (moistureSensorValue >= 300)) powerLEDs(3, leds, ledsN);
    if ((moistureSensorValue < 300) && (moistureSensorValue >= 200)) powerLEDs(2, leds, ledsN);
    if (moistureSensorValue < 200) powerLEDs(1, leds, ledsN);
}

void powerPump() {
    if (moistureSensorValue > MOISTURE_LEVEL_PUMP && moistureSensorValue < IGNORE_SENSOR_LEVEL_COSIDER_NOT_MOUNTED) {
        digitalWrite(PUMP_PIN, HIGH);
        delay(PUMP_POWER_TIME);
        digitalWrite(PUMP_PIN, LOW);
        lastWateringTime = millis();
    }
}

void setup() {
    initUSB();
    initLEDs(leds, ledsN);
    pinMode(PUMP_PIN, OUTPUT);
}

void loop() {
    time = millis();
    println("Time: ", time, " last watering time ", lastWateringTime, " time since last watering ",
            time - lastWateringTime);
    if (((time - lastWateringTime) < TIME_TO_WAIT_AFTER_WATERING_MILLIS) && (lastWateringTime > 0)) {
        println("Will wait because just watered ", time - lastWateringTime);
        waitingLEDs(leds, ledsN);
    } else {
        delay(1000);
        readSensor();
        showMoistureOnLEDs();
        powerPump();
    }
}
